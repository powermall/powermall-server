const express = require('express');
const app = express();
require("colors");
const cors = require('cors');
const fileUpload = require('express-fileupload');
const { MongoClient } = require('mongodb');
const ObjectID = require('mongodb').ObjectId;
const PORT = process.env.PORT || 5000;
const ObjectId = require('mongodb').ObjectId;
const SSLCommerzPayment = require('sslcommerz');
const { v4: uuid } = require("uuid");
const axios = require("axios");
const fs = require("fs");
const path = require("path");
const json2csv = require('json2csv').parse;
// const corsOptions = {
//     origin: 'http://localhost:3000',
//     credentials: true,            //access-control-allow-credentials:true
//     optionSuccessStatus: 200
// }
app.use(cors());
// app.use(cors());
app.use(express.json());
app.use(fileUpload());
app.use(express.urlencoded({ extended: true }));

const uri = "mongodb+srv://powermall:2022Powermall@cluster0.qiqi4.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";



const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

client.connect(err => {
    const db = client.db("powermall");
    const userCollection = db.collection("users");
    const productsCollection = db.collection("products");
    const brandsCollection = db.collection("brands");
    const categoryCollection = db.collection("category");
    const logosCollection = db.collection("logos");
    const headerBannersCollection = db.collection("header-banner");
    const saleBannersCollection = db.collection("sale-banner");
    const recomendedBannersCollection = db.collection("recomended-banner");
    const informationsCollection = db.collection("information");
    const orderStatusCollection = db.collection("orderstatus");
    const shippingsCollection = db.collection("shipping");
    const couponsCollection = db.collection("coupons");
    const refundsCollection = db.collection("refunds");
    const featuresCollection = db.collection("features");
    const notificationsCollection = db.collection("notification");
    const seoCollection = db.collection("seo");
    const faviconsCollection = db.collection("favicon");
    const aboutusCollection = db.collection("aboutus");
    const policyCollection = db.collection("policy");
    const cookieCollection = db.collection("cookie");
    const shopWithUsCollection = db.collection("shop-with-us");
    const termsAndConditionsCollection = db.collection("terms-and-conditions");
    const shippingAndDeliveryCollection = db.collection("shipping-and-delivery");
    const paymentMethodCollection = db.collection("payment-method");
    const applinkCollection = db.collection("applink");
    const orderCollection = db.collection("orders");
    const messageCollection = db.collection("messages");
    const socilalinksCollection = db.collection("social-links");
    const copyrightCollection = db.collection("copy-right");
    const rolesCollection = db.collection('account-roles');
    const noticeCollection = db.collection('notice');
    const wishlistCollection = db.collection('wishlist');

    app.get('/', (req, res) => {
        res.send('Welcome to our powermall');
    });

    // -------------- amar pay intregation  start --------------- //


    app.post("/payment", async (req, res, next) => {
        const { name, mobile, address, email, price, cart, deliveryMethod, status, orderID, date, time, refund} = req.body;

        const formData = {
            cus_name: name,
            name: name,
            cus_email: email,
            email: email,
            cus_phone: mobile,
            mobile: mobile,
            amount: price,
            price: price,
            tran_id: uuid(),
            // signature_key: "dbb74894e82415a2f7ff0ec3a97e4183",
            signature_key: "a04bbcfb93ea3d5ffcc46bd8de68fdef",
            // store_id: "aamarpaytest",
            store_id: "powermall",
            currency: "BDT",
            desc: "order",
            cus_add1: address,
            address: address,
            cus_add2: "Dhaka",
            cus_city: "Dhaka",
            cus_country: "Bangladesh",
            status: status,
            cus_cart: cart,
            cart: cart,
            orderID: orderID,
            date: date,
            time: time,
            refund: "Refund",
            deliveryMethod: deliveryMethod,
            success_url: "https://powermall-api.herokuapp.com/success",
            fail_url: "https://powermall-api.herokuapp.com/fail",
            cancel_url: "https://powermall-api.herokuapp.com/cancel",
            type: "json", //This is must required for JSON request
            paymentType: "Pre Payment",
        };
        const { data } = await axios.post(
            "https://secure.aamarpay.com/jsonpost.php",
            formData
        );
        if (data.result !== "true") {
            let errorMessage = "";
            for (let key in data) {
                errorMessage += data[key] + ". ";
            }
            return res.render("error", {
                title: "Error",
                errorMessage,
            });
        }

        await orderCollection.insertOne(formData);

        res.send(data.payment_url)
    });

    app.post("/success", async (req, res, next) => {
        // console.log(req.body)
        // await orderCollection.updateOne(req.body, { upsert: true })

        await orderCollection.updateOne({ tran_id: req.body.mer_txnid }, {
            $set: req.body
        }, { upsert: true })

        // res.send(req.body);
        res.redirect('https://powermall.com.bd/order-successful')
        // location.replace("http://localhost:3000/order-successful")
    });
    app.post("/fail", async (req, res, next) => {
        await orderCollection.deleteOne({ tran_id: req.body.mer_txnid })
        res.redirect('https://powermall.com.bd');
    });
    app.post("/cancel", async (req, res, next) => {
        await orderCollection.deleteOne({ tran_id: req.body.mer_txnid })
        res.redirect('https://powermall.com.bd');
    });



    // -------------- amar pay intregation  end --------------- //

    // ------------ add products ------------ //

    app.get('/products', async (req, res) => {

        const categoryValue = req.query.category;
        const brandValue = req.query.brand;
        const category = categoryValue?.charAt(0).toUpperCase() + categoryValue?.slice(1);
        const brand = brandValue?.charAt(0).toUpperCase() + brandValue?.slice(1);

        if (category) {
            const result = await productsCollection.find({ category }).toArray();
            res.send(result);
        } else if (brand) {
            const result = await productsCollection.find({ brand }).toArray();
            res.send(result);
        } else {
            const data = await productsCollection.find({}).toArray();
            res.json(data);
        }

    });

    app.get('/products/csv', async (req, res) => {
        const dateTime = new Date().toISOString().slice(-24).replace(/\D/g,
            '').slice(0, 14);

        const filePath = path.join(__dirname, "./", "public", "exports", "csv-" + dateTime + ".csv");

        let csv;

        const products = await productsCollection.find({}).toArray();

        const fields = ['_id', 'name', 'price', 'discount', 'warenty', 'quantity', 'description', 'moreInformation', 'ytLink', 'sku', 'category', 'brand', 'meta', 'color', 'subCategory', 'delivery', 'status', 'time', 'deliveryOption', 'storePickup', 'shippingCharge', 'img'];

        try {
            csv = json2csv(products, { fields });
        } catch (err) {
            return res.status(500).json({ err });
        }

        fs.writeFile(filePath, csv, function (err) {
            if (err) {
                return res.json(err).status(500);
            }
            else {
                setTimeout(function () {
                    fs.unlink(filePath, function (err) { // delete this file after 30 seconds
                        if (err) {
                            console.error(err);
                        }
                        console.log('File has been Deleted');
                    });

                }, 30000);
                res.download(filePath);
            }
        })
    })

    app.get('/products/:_id', async (req, res) => {

        const _id = req.params._id;
        const query = { _id: ObjectID(_id) };
        const data = await productsCollection.findOne(query);
        res.json(data);

    });

    app.delete('/products/:_id', async (req, res) => {

        const _id = req.params._id;
        const query = { _id: ObjectID(_id) };
        const data = await productsCollection.deleteOne(query);
        res.json(data);

    });

    app.put('/products/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const options = { upsert: true };
        const filter = { _id: ObjectID(_id) };
        const updatedDoc = { $set: data };
        const result = await productsCollection.updateOne(filter, updatedDoc, options);
        res.send(result);

    });

    app.post('/products', async (req, res) => {

        const products = req.body;
        const options = { ordered: true };

        if (Array.isArray(products)) {
            const result = await productsCollection.insertMany(products, options);
            res.send(result);
        } else {
            const result = await productsCollection.insertOne(products);
            res.send(result);
        }

    });

    // ------------ add brands ------------ //

    app.get('/brands', async (req, res) => {

        const result = await brandsCollection.find({}).toArray();
        res.json(result);

    });

    app.get('/brands/:_id', async (req, res) => {

        const _id = req.params._id;
        const query = { _id: ObjectID(_id) };
        const result = await brandsCollection.findOne(query);
        res.json(result);

    });

    app.post('/brands', async (req, res) => {

        const brand = req.body;
        const result = await brandsCollection.insertOne(brand);
        res.send(result);

    });

    app.delete('/brands/:_id', async (req, res) => {

        const _id = req.params._id;
        const query = { _id: ObjectID(_id) };
        const result = await brandsCollection.deleteOne(query);
        res.json(result);

    });

    // ------------ add category ------------ //

    app.get('/category', async (req, res) => {

        const category = req.query.name;
        if (category) {
            const result = await categoryCollection.findOne({ category });
            res.json(result);
        } else {
            const result = await categoryCollection.find({}).toArray();
            res.json(result);
        }

    });

    app.get('/category/:_id', async (req, res) => {

        const _id = req.params._id;
        const query = { _id: ObjectID(_id) };
        const result = await categoryCollection.findOne(query);
        res.json(result);

    });

    app.post('/category', async (req, res) => {

        const category = req.body;
        const result = await categoryCollection.insertOne(category);
        res.send(result);

    });

    app.put('/category/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const options = { upsert: true };
        const filter = { _id: ObjectID(_id) };
        const updatedDoc = { $set: data };
        const resutlt = await categoryCollection.updateOne(filter, updatedDoc, options);
        res.send(resutlt);

    });

    app.delete('/category/:_id', async (req, res) => {

        const _id = req.params._id;
        const query = { _id: ObjectID(_id) };
        const result = await categoryCollection.deleteOne(query);
        res.json(result);

    });

    // ------------ logo upload ------------ //

    app.post('/logo', async (req, res) => {

        const logo = req.body;
        const result = await logosCollection.insertOne(logo);
        res.send(result);

    });

    app.get('/logo', async (req, res) => {

        const result = await logosCollection.find({}).toArray();
        res.send(result);

    });

    app.delete('/logo', async (req, res) => {

        const query = { name: 'logo' };
        const result = await logosCollection.deleteMany(query);
        res.send(result);

    });

    // ------------ header-banner upload ------------ //

    app.post('/headerbanner', async (req, res) => {

        const data = req.body;
        const result = await headerBannersCollection.insertOne(data);
        res.send(result);

    });

    app.put('/headerbanner/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const options = { upsert: true };
        const filter = { _id: ObjectID(_id) };
        const updatedDoc = { $set: data };
        const result = await headerBannersCollection.updateOne(filter, updatedDoc, options);
        res.send(result);

    });

    app.get('/headerbanner', async (req, res) => {

        const result = await headerBannersCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/headerbanner/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await headerBannersCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    // ------------ special-sale banner upload ------------ //

    app.post('/salebanner', async (req, res) => {

        const data = req.body;
        const result = await saleBannersCollection.insertOne(data);
        res.send(result);

    });

    app.put('/salebanner/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const options = { upsert: true };
        const filter = { _id: ObjectID(_id) };
        const updatedDoc = { $set: data };

        const result = await saleBannersCollection.updateOne(filter, updatedDoc, options);
        res.send(result);

    });

    app.get('/salebanner', async (req, res) => {

        const result = await saleBannersCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/salebanner/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await saleBannersCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    // ------------ recomended banner upload ------------ //

    app.post('/recomendedbanner', async (req, res) => {

        const data = req.body;
        const result = await recomendedBannersCollection.insertOne(data);
        res.send(result);

    });

    app.put('/recomendedbanner/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const options = { upsert: true };
        const filter = { _id: ObjectID(_id) };
        const updatedDoc = { $set: data };
        const result = await recomendedBannersCollection.updateOne(filter, updatedDoc, options);
        res.send(result);

    });

    app.get('/recomendedbanner', async (req, res) => {

        const result = await recomendedBannersCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/recomendedbanner/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await recomendedBannersCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    // ------------ information update ------------ //

    app.post('/information', async (req, res) => {

        const information = req.body;
        const result = await informationsCollection.insertOne(information);
        res.send(result);

    });

    app.get('/information', async (req, res) => {

        const result = await informationsCollection.find({}).toArray();
        res.send(result);

    });

    app.put('/information/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const query = { _id: ObjectID(_id) };
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = await informationsCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    // ------------ order status ------------ //

    app.post('/orderstatus', async (req, res) => {

        const order = req.body;
        const result = await orderStatusCollection.insertOne(order);
        res.send(result);

    });

    app.get('/orderstatus', async (req, res) => {

        const result = await orderStatusCollection.find({}).toArray();
        res.send(result);

    });

    app.put('/orderstatus/:_id', async (req, res) => {

        const _id = req.params._id;
        const query = { _id };
        const status = req.body.status;
        const options = { upsert: true };
        const updateDoc = {
            $set: {
                status
            },
        };

        const result = await orderStatusCollection.updateOne(query, updateDoc, options);
        res.send(result);

    });

    app.get('/orderstatus/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await orderStatusCollection.findOne({ _id });
        res.send(result);

    });

    app.delete('/orderstatus/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await orderStatusCollection.deleteOne({ _id });
        res.send(result);

    });

    // ------------ shipping ------------ //

    app.post('/shipping', async (req, res) => {

        const shipping = req.body;
        const result = await shippingsCollection.insertOne(shipping);
        res.send(result);

    });

    app.get('/shipping', async (req, res) => {

        const result = await shippingsCollection.find({}).toArray();
        res.send(result);

    });

    app.put('/shipping/:_id', async (req, res) => {

        const _id = req.params._id;
        const query = { _id };
        const status = req.body.status;
        const options = { upsert: true };
        const updateDoc = {
            $set: {
                status
            },
        };

        const result = await shippingsCollection.updateOne(query, updateDoc, options);
        res.send(result);

    });

    app.delete('/shipping/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await shippingsCollection.deleteOne({ _id });
        res.send(result);

    });

    // ------------ add coupons ------------ //

    app.post('/coupons', async (req, res) => {

        const data = req.body;
        const result = await couponsCollection.insertOne(data);
        res.send(result);

    });

    app.get('/coupons', async (req, res) => {

        const result = await couponsCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/coupons/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await couponsCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    app.put('/coupons/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const query = { _id: ObjectID(_id) };
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = couponsCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    app.delete('/coupons/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await couponsCollection.deleteOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    // ------------ add features ------------ //

    app.post('/feature', async (req, res) => {

        const data = req.body;
        const result = await featuresCollection.insertOne(data);
        res.send(result);

    });

    app.delete('/feature/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await featuresCollection.deleteOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    app.put('/feature/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const query = { _id: ObjectID(_id) };
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = await featuresCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    app.get('/feature', async (req, res) => {

        const result = await featuresCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/feature/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await featuresCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    // ------------ add notification ------------ //

    app.post('/notification', async (req, res) => {
        const data = req.body;
        const result = await notificationsCollection.insertOne(data);
        res.send(result);

    });

    app.get('/notification/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await notificationsCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);
    });
    app.delete('/notification/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await notificationsCollection.deleteOne({ _id: ObjectID(_id) });
        res.send(result);
    });
    app.get('/notification', async (req, res) => {
        const result = await notificationsCollection.find({}).toArray();
        res.send(result);
    });

    app.put('/notification/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const query = { _id: ObjectID(_id) };
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = await notificationsCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    //add wishlist 
    app.get('/wishlist', async (req, res) => {
        const result = await wishlistCollection.find({}).toArray()
        res.send(result)
    })
    app.get('/wishlist/:id', async (req, res) => {
        const _id = req.params.id;
        const result = await wishlistCollection.findOne({ _id })
        res.send(result)
    })

    app.post('/wishlist', async (req, res) => {
        const data = req.body;
        const result = await wishlistCollection.insertOne(data)
        res.send(result)
    })

    app.delete('/wishlist/:_id', async (req, res) => {
        const _id = req.params._id;
        const result = await wishlistCollection.deleteOne({ _id });
        res.send(result);
    })
    app.put('/wishlist/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const query = { _id };
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = await wishlistCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    // -----  add notice  
    app.get('/notice', async (req, res) => {
        const result = await noticeCollection.find({}).toArray()
        res.json(result)
    })

    app.post('/notice', async (req, res) => {
        const data = req.body;
        const result = await noticeCollection.insertOne(data);
        res.json(result);
    })

    app.delete('/notice/:_id', async (req, res) => {
        const _id = req.params._id;
        const result = await noticeCollection.deleteOne({ _id });
        res.send(result);
    })

    app.put('/notice/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const query = { _id: ObjectID(_id) };
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = await noticeCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });


    // ------------ add seo ------------ //

    app.post('/seo', async (req, res) => {

        const data = req.body;
        const result = await seoCollection.insertOne(data);
        res.send(result);

    });

    app.get('/seo', async (req, res) => {

        const result = await seoCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/seo/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await seoCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    app.put('/seo/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const query = { _id: ObjectID(_id) };
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = await seoCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    // ------------ refunds ------------ //

    app.post('/refunds', async (req, res) => {

        const data = req.body;
        const result = refundsCollection.insertOne(data);
        res.send(result);

    });

    app.get('/refunds', async (req, res) => {

        const result = await refundsCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/refunds/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await refundsCollection.findOne({ _id });
        res.send(result);

    });

    app.delete('/refunds/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await refundsCollection.deleteOne({ _id });
        res.send(result);

    });

    app.put('/refunds/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const query = { _id };
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = refundsCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    // ------------ favicon upload ------------ //

    app.post('/favicon', async (req, res) => {

        const data = req.body;
        const result = await faviconsCollection.insertOne(data);
        res.send(result);

    });

    app.get('/favicon', async (req, res) => {

        const result = await faviconsCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/favicon/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await faviconsCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    app.put('/favicon/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const query = { _id: ObjectID(_id) };
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = await faviconsCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    // ------------ About Us ------------ //

    app.post('/aboutus', async (req, res) => {

        const data = req.body;
        const result = await aboutusCollection.insertOne(data);
        res.send(result);

    });

    app.get('/aboutus', async (req, res) => {

        const result = await aboutusCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/aboutus/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await aboutusCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    app.put('/aboutus/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const query = { _id: ObjectID(_id) };
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = await aboutusCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    // ------------ Privacy Policy ------------ //

    app.post('/policy', async (req, res) => {

        const data = req.body;
        const result = await policyCollection.insertOne(data);
        res.send(result);

    });

    app.get('/policy', async (req, res) => {

        const result = await policyCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/policy/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await policyCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    app.put('/policy/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const query = { _id: ObjectID(_id) };
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = await policyCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    // ------------ Cookie Policy ------------ //

    app.post('/cookie', async (req, res) => {

        const data = req.body;
        const result = await cookieCollection.insertOne(data);
        res.send(result);

    });

    app.get('/cookie', async (req, res) => {

        const result = await cookieCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/cookie/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await cookieCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    app.put('/cookie/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const query = { _id: ObjectID(_id) };
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = await cookieCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    // ------------ shop with us ------------ //

    app.post('/shopwithus', async (req, res) => {

        const data = req.body;
        const result = await shopWithUsCollection.insertOne(data);
        res.send(result);

    });

    app.get('/shopwithus', async (req, res) => {

        const result = await shopWithUsCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/shopwithus/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await shopWithUsCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    app.put('/shopwithus/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const query = { _id: ObjectID(_id) };
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = await shopWithUsCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    // ------------ terms & condition ------------ //

    app.post('/termsandconditions', async (req, res) => {

        const data = req.body;
        const result = await termsAndConditionsCollection.insertOne(data);
        res.send(result);

    });

    app.get('/termsandconditions', async (req, res) => {

        const result = await termsAndConditionsCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/termsandconditions/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await termsAndConditionsCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    app.put('/termsandconditions/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const query = { _id: ObjectID(_id) };
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = await termsAndConditionsCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    // ------------ Shipping And Delivery ------------ //

    app.post('/shippinganddelivery', async (req, res) => {

        const data = req.body;
        const result = await shippingAndDeliveryCollection.insertOne(data);
        res.send(result);

    });

    app.get('/shippinganddelivery', async (req, res) => {

        const result = await shippingAndDeliveryCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/shippinganddelivery/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await shippingAndDeliveryCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    app.put('/shippinganddelivery/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const query = { _id: ObjectID(_id) };
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = await shippingAndDeliveryCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    // ------------ payment method ------------ //

    app.post('/paymentmethod', async (req, res) => {

        const data = req.body;
        const result = await paymentMethodCollection.insertOne(data);
        res.send(result);

    });

    app.get('/paymentmethod', async (req, res) => {

        const result = await paymentMethodCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/paymentmethod/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await paymentMethodCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    app.put('/paymentmethod/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const query = { _id: ObjectID(_id) };
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = await paymentMethodCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    // ------------ app link ------------ //

    app.post('/applink', async (req, res) => {

        const data = req.body;
        const result = await applinkCollection.insertOne(data);
        res.send(result);

    });

    app.get('/applink', async (req, res) => {

        const result = await applinkCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/applink/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await applinkCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    app.put('/applink/:_id', async (req, res) => {

        const _id = req.params._id;
        const data = req.body;
        const query = { _id: ObjectID(_id) };
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = await applinkCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    // ------------ app link ------------ //

    app.post('/sociallinks', async (req, res) => {

        const data = req.body;
        const result = await socilalinksCollection.insertOne(data);
        res.send(result);

    });

    app.get('/sociallinks', async (req, res) => {

        const result = await socilalinksCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/sociallinks/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await socilalinksCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    app.put('/sociallinks/:_id', async (req, res) => {

        const _id = req.params._id;
        const query = { _id: ObjectID(_id) };
        const data = req.body;
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = await socilalinksCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    // ------------ app link ------------ //

    app.post('/copyright', async (req, res) => {

        const data = req.body;
        const result = await copyrightCollection.insertOne(data);
        res.send(result);

    });

    app.get('/copyright', async (req, res) => {

        const result = await copyrightCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/copyright/:_id', async (req, res) => {

        const _id = req.params._id;
        const result = await copyrightCollection.findOne({ _id: ObjectID(_id) });
        res.send(result);

    });

    app.put('/copyright/:_id', async (req, res) => {

        const _id = req.params._id;
        const query = { _id: ObjectID(_id) };
        const data = req.body;
        const options = { upsert: true };
        const updatedDoc = { $set: data };
        const result = await copyrightCollection.updateOne(query, updatedDoc, options);
        res.send(result);

    });

    app.post('/roles', async (req, res) => {

        const data = req.body;
        const result = await rolesCollection.insertOne(data);
        res.send(result);

    });

    app.get('/roles', async (req, res) => {

        const result = await rolesCollection.find({}).toArray();
        res.send(result);

    });

    app.get('/roles/:email', async (req, res) => {

        const email = req.params.email;
        const result = await rolesCollection.findOne({ email });
        res.send(result);

    });

    //abtahis code 


    // =================================================


    //GET (users)
    app.get('/users', async (req, res) => {
        const email = req.query.email;
        if (email) {
            const result = await userCollection.findOne({ email: email });
            res.send(result);
        } else {
            const result = await userCollection.find({}).toArray();
            res.send(result);
        }

    });

    app.get('/users/:id', async (req, res) => {
        const id = req.params.id;
        const result = await userCollection.findOne({ _id: new ObjectId(id) });
        res.send(result);
    });

    //POST(users)
    app.post('/users', async (req, res) => {
        const newUser = req.body;
        const result = await userCollection.insertOne(newUser);
        res.send(result);
    });

    //DELETE(users)
    app.delete('/users/:id', async (req, res) => {
        const id = req.params.id;
        const result = await userCollection.deleteOne({ _id: new ObjectId(id) });
        res.send(result);
    });

    //UPDATE (users)
    app.put('/users/:id', async (req, res) => {
        const id = req.params.id;
        const newStatus = req.body;
        const query = { _id: new ObjectId(id) };
        const options = { upsert: true };
        const updateDoc = { $set: newStatus };
        const result = await userCollection.updateOne(query, updateDoc, options);
        res.send(result);
    });


    // ==================================================================
    // orders api 
    //GET (orders)
    app.get('/orders', async (req, res) => {
        const email = req.query.email;
        if (email) {
            const result = await orderCollection.find({ email: email }).toArray();
            res.send(result);
        } else {
            const result = await orderCollection.find({}).toArray();
            res.send(result);
        }

    });

    app.get('/orders/:id', async (req, res) => {
        const id = req.params.id;
        const result = await orderCollection.findOne({ _id: new ObjectId(id) });
        res.send(result);
    });

    //POST(orders)
    app.post('/orders', async (req, res) => {
        const newOrder = req.body;
        const result = await orderCollection.insertOne(newOrder);
        res.send(result);
    });

    //DELETE(orders)
    app.delete('/orders/:id', async (req, res) => {
        const id = req.params.id;
        const result = await orderCollection.deleteOne({ _id: new ObjectId(id) });
        res.send(result);
    });

    //UPDATE (orders)
    app.put('/orders/:id', async (req, res) => {
        const id = req.params.id;
        const newStatus = req.body;
        const query = { _id: new ObjectId(id) };
        const options = { upsert: true };
        const updateDoc = { $set: newStatus };
        const result = await orderCollection.updateOne(query, updateDoc, options);
        res.send(result);
    });




    //ssl commerce
    // // Initialize payment
    // app.post('/init', async (req, res) => {
    //     // const productInfo = {
    //     //     total_amount: 2424,
    //     //     currency: 'BDT',
    //     //     tran_id: uuidv4(),
    //     //     success_url: 'https://powermall-api.herokuapp.com/success',
    //     //     fail_url: 'https://powermall-api.herokuapp.com/failure',
    //     //     cancel_url: 'https://powermall-api.herokuapp.com/cancel',
    //     //     ipn_url: 'http://yoursite.com/ipn',
    //     //     shipping_method: 'Courier',
    //     //     // cart:req.body.cart,
    //     //     product_name: 'Computer.',
    //     //     product_category: 'Electronic',
    //     //     product_profile: 'general',
    //     //     cus_name: 'ssaf',
    //     //     cus_email: "saf",
    //     //     address: "fsasfas",
    //     //     cus_add1: 'Dhaka',
    //     //     cus_add2: 'Dhaka',
    //     //     cus_city: 'Dhaka',
    //     //     cus_state: 'Dhaka',
    //     //     cus_postcode: '1000',
    //     //     cus_country: 'Bangladesh',
    //     //     cus_phone: '3124124',
    //     //     cus_fax: '01711111111',
    //     //     ship_name: 'saf',
    //     //     ship_add1: 'Dhaka',
    //     //     ship_add2: 'Dhaka',
    //     //     ship_city: 'Dhaka',
    //     //     ship_state: 'Dhaka',
    //     //     ship_postcode: 1000,
    //     //     ship_country: 'Bangladesh',
    //     //     multi_card_name: 'mastercard',
    //     //     value_a: 'ref001_A',
    //     //     value_b: 'ref002_B',
    //     //     value_c: 'ref003_C',
    //     //     value_d: 'ref004_D',
    //     //     paymentStatus: 'pending',
    //     //     status: 'pending',
    //     //     // date:req.body.date,
    //     //     // time:req.body.time,
    //     // };
    //     const productInfo = {
    //         total_amount: req.body.price,
    //         currency: 'BDT',
    //         tran_id: uuidv4(),
    //         success_url: 'https://powermall-api.herokuapp.com/success',
    //         fail_url: 'https://powermall-api.herokuapp.com/failure',
    //         cancel_url: 'https://powermall-api.herokuapp.com/cancel',
    //         ipn_url: 'http://yoursite.com/ipn',
    //         shipping_method: 'Courier',
    //         // cart:req.body.cart,
    //         product_name: 'Computer.',
    //         product_category: 'Electronic',
    //         product_profile: 'general',
    //         cus_name: req.body.name,
    //         cus_email: req.body.email,
    //         address: req.body.address,
    //         cus_add1: 'Dhaka',
    //         cus_add2: 'Dhaka',
    //         cus_city: 'Dhaka',
    //         cus_state: 'Dhaka',
    //         cus_postcode: '1000',
    //         cus_country: 'Bangladesh',
    //         cus_phone: req.body.mobile,
    //         cus_fax: '01711111111',
    //         ship_name: req.body.name,
    //         ship_add1: 'Dhaka',
    //         ship_add2: 'Dhaka',
    //         ship_city: 'Dhaka',
    //         ship_state: 'Dhaka',
    //         ship_postcode: 1000,
    //         ship_country: 'Bangladesh',
    //         multi_card_name: 'mastercard',
    //         value_a: 'ref001_A',
    //         value_b: 'ref002_B',
    //         value_c: 'ref003_C',
    //         value_d: 'ref004_D',
    //         paymentStatus: 'pending',
    //         status: 'pending',
    //         date:req.body.date,
    //         time:req.body.time,
    //     };

    //     // Insert order info
    //     // console.log(req.body)
    //     const result = await orderCollection.insertOne(productInfo);

    //     const sslcommer = new SSLCommerzPayment("dropd61c4a01619c7f","dropd61c4a01619c7f@ssl", false) //true for live default false for sandbox
    //     sslcommer.init(productInfo).then(data => {

    //         if (data.GatewayPageURL) {
    //             res.json(data.GatewayPageURL)
    //         }
    //         else {
    //             return res.status(400).json({
    //                 message: "SSL session was not successful"
    //             })
    //         }

    //     });
    // });



    // app.post("/success", async (req, res) => {
    //     const options = { upsert: true };
    //     const updateDoc = { $set: newStatus };
    //     const result = await orderCollection.updateOne(query, updateDoc, options);
    //     const result = await orderCollection.updateOne({ tran_id: req.body.tran_id }, {
    //         $set: {
    //             val_id: req.body.val_id,
    //             paymentStatus: 'Paid'
    //         }
    //     }, { upsert: true } )

    //     res.redirect(`http://localhost:3000/order-successful`)
    //     res.status(200).redirect(`http://localhost:3000/order-successful`)
    //     res.json(req.body)
    // })


    // app.post("/failure", async (req, res) => {
    //     const result = await orderCollection.deleteOne({ tran_id: req.body.tran_id })
    //     res.redirect(`http://localhost:3000`)
    // })
    // app.post("/cancel", async (req, res) => {
    //     const result = await orderCollection.deleteOne({ tran_id: req.body.tran_id })
    //     res.redirect(`http://localhost:3000`)
    // })





    //GET (messages)
    app.get('/messages', async (req, res) => {
        const result = await messageCollection.find({}).toArray();
        res.send(result);
    });


    app.get('/messages/:id', async (req, res) => {
        const id = req.params.id;
        const result = await messageCollection.findOne({ _id: new ObjectId(id) });
        res.send(result);
    });

    //POST(messages)
    app.post('/messages', async (req, res) => {
        const newMessage = req.body;
        const result = await messageCollection.insertOne(newMessage);
        res.send(result);
    });

    //DELETE(users)
    app.delete('/messages/:id', async (req, res) => {
        const id = req.params.id;
        const result = await messageCollection.deleteOne({ _id: new ObjectId(id) });
        res.send(result);
    });
});


app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
});